import {
  registerSettings,
  settings,
  getSetting,
  registerMenu,
} from "./module/settings";
import { EntitySearchItem, enabledDocumentTypes } from "./module/searchLib";

import { FilterList } from "./module/filterList";
import { SheetFilters } from "./module/sheetFilters";

import { importSystemIntegration } from "./module/systemIntegration";
import { registerTinyMCEPlugin } from "./module/tinyMCEPlugin";

import { SearchApp } from "./module/searchApp";
import { QuickInsert, loadSearchIndex } from "./module/core";
import { SearchFilterCollection } from "./module/searchFilters";
import { IndexingSettings } from "./module/indexingSettings";
import { resolveAfter } from "./module/utils";

function quickInsertDisabled(): boolean {
  return !game.user?.isGM && getSetting(settings.GM_ONLY);
}

// Client is currently reindexing?
let reIndexing = false;

Hooks.once("init", async function () {
  registerMenu({
    menu: "indexingSettings",
    name: "QUICKINSERT.SettingsIndexingSettings",
    label: "QUICKINSERT.SettingsIndexingSettingsLabel",
    icon: "fas fa-search",
    type: IndexingSettings,
    restricted: false,
  });
  registerMenu({
    menu: "filterMenu",
    name: "QUICKINSERT.SettingsFilterMenu",
    label: "QUICKINSERT.SettingsFilterMenuLabel",
    icon: "fas fa-filter",
    type: FilterList,
    restricted: false,
  });
  registerSettings({
    [settings.FILTERS_WORLD]: () => {
      if (quickInsertDisabled()) return;
      QuickInsert.filters.loadSave();
    },
    [settings.FILTERS_CLIENT]: () => {
      if (quickInsertDisabled()) return;
      QuickInsert.filters.loadSave();
    },
    [settings.INDEXING_DISABLED]: async () => {
      if (quickInsertDisabled()) return;
      // Active users will start reindexing in deterministic order, once per 300ms

      if (reIndexing) return;
      reIndexing = true;
      if (game.users) {
        const order = [...game.users.contents]
          .filter((u) => u.active)
          .map((u) => u.id)
          .indexOf(game.userId);
        await resolveAfter(order * 300);
      }
      await QuickInsert.forceIndex();
      reIndexing = false;
    },
  });

  KeybindLib.register("quick-insert", settings.KEY_BIND, {
    name: "QUICKINSERT.SettingsQuickOpen",
    hint: "QUICKINSERT.SettingsQuickOpenHint",
    default: "Ctrl + Space",
    config: true,
  });
});

Hooks.once("ready", function () {
  if (quickInsertDisabled()) return;
  console.log("Quick Insert | Initializing...");

  // Initialize application base
  QuickInsert.filters = new SearchFilterCollection();
  QuickInsert.app = new SearchApp();

  registerTinyMCEPlugin();

  importSystemIntegration().then((systemIntegration) => {
    if (systemIntegration) {
      QuickInsert.systemIntegration = systemIntegration;
      QuickInsert.systemIntegration.init();

      if (QuickInsert.systemIntegration.defaultSheetFilters) {
        registerMenu({
          menu: "sheetFilters",
          name: "QUICKINSERT.SettingsSheetFilters",
          label: "QUICKINSERT.SettingsSheetFiltersLabel",
          icon: "fas fa-filter",
          type: SheetFilters,
          restricted: false,
        });
      }
    }
  });

  document.addEventListener("keydown", (evt) => {
    if (QuickInsert.matchBoundKeyEvent(evt)) {
      evt.stopPropagation();
      evt.preventDefault();
      QuickInsert.toggle();
    }
  });

  enabledDocumentTypes().forEach((type) => {
    Hooks.on(`create${type}`, (document: AnyDocument) => {
      if (document.parent || !document.visible) return;
      QuickInsert.searchLib?.addItem(EntitySearchItem.fromDocument(document));
    });
    Hooks.on(`update${type}`, (document: AnyDocument) => {
      if (document.parent) return;
      if (!document.visible) {
        QuickInsert.searchLib?.removeItem(document.uuid);
        return;
      }
      QuickInsert.searchLib?.replaceItem(
        EntitySearchItem.fromDocument(document)
      );
    });
    Hooks.on(`delete${type}`, (document: AnyDocument) => {
      if (document.parent) return;
      QuickInsert.searchLib?.removeItem(document.uuid);
    });
  });

  console.log("Quick Insert | Search Application ready");

  const indexDelay = getSetting(settings.AUTOMATIC_INDEXING);
  if (indexDelay != -1) {
    setTimeout(() => {
      console.log("Quick Insert | Automatic indexing initiated");
      loadSearchIndex(false);
    }, indexDelay);
  }
});

Hooks.on("renderSceneControls", (controls: unknown, html: JQuery) => {
  if (!getSetting(settings.SEARCH_BUTTON)) return;
  const searchBtn = $(
    `<li class="scene-control" title="Quick Insert" class="quick-insert-open">
          <i class="fas fa-search"></i>
      </li>`
  );
  html.append(searchBtn);
  searchBtn.on("click", () => QuickInsert.open());
});

// Exports and API usage
//@ts-ignore
globalThis.QuickInsert = QuickInsert;
export { SearchContext } from "./module/contexts";
export { QuickInsert };
