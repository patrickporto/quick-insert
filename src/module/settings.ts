const MODULE_NAME = "quick-insert";
export const SAVE_SETTINGS_REVISION = 1;

export enum settings {
  // QUICKOPEN = "quickOpen", // dead setting
  ENABLE_GLOBAL_CONTEXT = "enableGlobalContext",
  INDEXING_DISABLED = "indexingDisabled",
  FILTERS_CLIENT = "filtersClient",
  FILTERS_WORLD = "filtersWorld",
  FILTERS_SHEETS = "filtersSheets",
  FILTERS_SHEETS_ENABLED = "filtersSheetsEnabled",
  GM_ONLY = "gmOnly",
  AUTOMATIC_INDEXING = "automaticIndexing",
  INDEX_TIMEOUT = "indexTimeout",
  SEARCH_BUTTON = "searchButton",
  KEY_BIND = "keyBind",
}

type Callback = () => void;

const moduleSettings = [
  {
    setting: settings.GM_ONLY,
    name: "QUICKINSERT.SettingsGmOnly",
    hint: "QUICKINSERT.SettingsGmOnlyHint",
    type: Boolean,
    default: false,
    scope: "world",
  },
  {
    setting: settings.FILTERS_SHEETS_ENABLED,
    name: "QUICKINSERT.SettingsFiltersSheetsEnabled",
    hint: "QUICKINSERT.SettingsFiltersSheetsEnabledHint",
    type: Boolean,
    default: true,
    scope: "world",
  },
  {
    setting: settings.AUTOMATIC_INDEXING,
    name: "QUICKINSERT.SettingsAutomaticIndexing",
    hint: "QUICKINSERT.SettingsAutomaticIndexingHint",
    type: Number,
    choices: {
      3000: "QUICKINSERT.SettingsAutomaticIndexing3s",
      5000: "QUICKINSERT.SettingsAutomaticIndexing5s",
      10000: "QUICKINSERT.SettingsAutomaticIndexing10s",
      "-1": "QUICKINSERT.SettingsAutomaticIndexingOnFirstOpen",
    },
    default: -1,
    scope: "client",
  },
  {
    setting: settings.INDEX_TIMEOUT,
    name: "QUICKINSERT.SettingsIndexTimeout",
    hint: "QUICKINSERT.SettingsIndexTimeoutHint",
    type: Number,
    choices: {
      1500: "QUICKINSERT.SettingsIndexTimeout1_5s",
      3000: "QUICKINSERT.SettingsIndexTimeout3s",
      7000: "QUICKINSERT.SettingsIndexTimeout7s",
      9500: "QUICKINSERT.SettingsIndexTimeou9_5s",
    },
    default: 1500,
    scope: "world",
  },
  {
    setting: settings.SEARCH_BUTTON,
    name: "QUICKINSERT.SettingsSearchButton",
    hint: "QUICKINSERT.SettingsSearchButtonHint",
    type: Boolean,
    default: false,
    scope: "client",
  },
  {
    setting: settings.ENABLE_GLOBAL_CONTEXT,
    name: "QUICKINSERT.SettingsEnableGlobalContext",
    hint: "QUICKINSERT.SettingsEnableGlobalContextHint",
    type: Boolean,
    default: true,
  },
  {
    setting: settings.INDEXING_DISABLED,
    name: "Things that have indexing disabled",
    type: Object,
    default: {
      entities: {
        Macro: [1, 2],
        Scene: [1, 2],
        Playlist: [1, 2],
        RollTable: [1, 2],
      },
      packs: {},
    },
    scope: "world",
    config: false, // Doesn't show up in config
  },
  {
    setting: settings.FILTERS_CLIENT,
    name: "Own filters",
    type: Object,
    default: {
      saveRev: SAVE_SETTINGS_REVISION,
      disabled: [],
      filters: [],
    },
    config: false, // Doesn't show up in config
  },
  {
    setting: settings.FILTERS_WORLD,
    name: "World filters",
    type: Object,
    default: {
      saveRev: SAVE_SETTINGS_REVISION,
      filters: [],
    },
    scope: "world",
    config: false, // Doesn't show up in config
  },
  {
    setting: settings.FILTERS_SHEETS,
    name: "Sheet filters",
    type: Object,
    default: {},
    scope: "world",
    config: false, // Doesn't show up in config
  },
];

function registerSetting(
  callback: Callback,
  { setting, ...options }: { setting: string }
): void {
  game.settings.register(MODULE_NAME, setting, {
    config: true,
    scope: "client",
    ...options,
    onChange: callback || undefined,
  });
}

export function registerSettings(
  callbacks: {
    [setting: string]: Callback;
  } = {}
): void {
  moduleSettings.forEach((item) => {
    registerSetting(callbacks[item.setting], item);
  });
}

export function getSetting(setting: settings | string): any {
  return game.settings.get(MODULE_NAME, setting as string);
}

export function setSetting(
  setting: settings | string,
  value: unknown
): Promise<unknown> {
  return game.settings.set(MODULE_NAME, setting as string, value);
}

export function registerMenu({
  menu,
  ...options
}: { menu: string } & ClientSettings.PartialMenuSetting): void {
  game.settings.registerMenu(MODULE_NAME, menu, options as any);
}
