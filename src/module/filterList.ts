import { FilterType, SearchFilter, SearchFilterConfig } from "./searchFilters";
import { i18n, randomId } from "./utils";
import { FilterEditor } from "./filterEditor";

import { QuickInsert } from "./core";

const typeIcons = {
  [FilterType.Default]: `<i class="fas fa-lock" title="Default filter"></i>`,
  [FilterType.World]: `<i class="fas fa-globe" title="World filter"></i>`,
  [FilterType.Client]: `<i class="fas fa-user" title="Client filter"></i>`,
};

function cloneFilterConfig(original: SearchFilterConfig): SearchFilterConfig {
  const res: SearchFilterConfig = {
    compendiums: "any",
    folders: "any",
    entities: "any",
  };

  if (typeof original.compendiums !== "string") {
    res.compendiums = [...original.compendiums];
  }
  if (typeof original.folders !== "string") {
    res.folders = [...original.folders];
  }
  if (typeof original.entities !== "string") {
    res.entities = [...original.entities];
  }

  return res;
}

export class FilterList extends FormApplication {
  filterEditors: { [key: string]: FilterEditor } = {};

  static get defaultOptions(): FormApplication.Options {
    return {
      ...super.defaultOptions,
      title: i18n("FilterListTitle"),
      id: "filter-list",
      template: "modules/quick-insert/templates/filter-list.hbs",
      resizable: true,
      height: 500,
      width: 350,
      scrollY: [".table-container"],
    };
  }

  getData(): any {
    return {
      filters: [
        ...QuickInsert.filters.filters.map((filter) => ({
          id: filter.id,
          icon: typeIcons[filter.type],
          tag: filter.tag,
          subTitle: filter.subTitle,
          disabled: filter.disabled,
          deletable:
            filter.type == FilterType.Client ||
            (filter.type == FilterType.World && game.user?.isGM),
        })),
      ],
    };
  }

  onFiltersUpdated = (): void => {
    this.render(true);
    Object.entries(this.filterEditors).forEach(([id, editor]) => {
      const filter = QuickInsert.filters.getFilter(id);
      if (filter) editor.filter = filter;

      editor.rendered && editor.render(true);
    });
  };

  render(force?: boolean, options?: Application.RenderOptions): Application {
    if (this._state <= 0) {
      Hooks.on("QuickInsert:FiltersUpdated", this.onFiltersUpdated);
    }
    return super.render(force, options) as Application;
  }

  close(): Promise<void> {
    Hooks.off("QuickInsert:FiltersUpdated", this.onFiltersUpdated);
    return super.close();
  }

  activateListeners(): void {
    this.element.find(".create-filter").on("click", () => {
      this.newFilter();
    });

    this.element.find("i.delete").on("click", (evt) => {
      const id = evt.target.closest("tr")?.dataset["id"];
      if (id) QuickInsert.filters.deleteFilter(id);
    });

    this.element.find("i.edit").on("click", (evt) => {
      const id = evt.target.closest("tr")?.dataset["id"];
      if (id) this.editFilter(id);
    });

    this.element.find("i.duplicate").on("click", (evt) => {
      const id = evt.target.closest("tr")?.dataset["id"];
      this.newFilter(QuickInsert.filters.filters.find((f) => f.id === id));
    });

    this.element.find("i.enable").on("click", (evt) => {
      const id = evt.target.closest("tr")?.dataset["id"];
      const filter = QuickInsert.filters.filters.find((f) => f.id === id);
      if (filter) filter.disabled = false;
      QuickInsert.filters.filtersChanged(FilterType.Client);
    });

    this.element.find("i.disable").on("click", (evt) => {
      const id = evt.target.closest("tr")?.dataset["id"];
      const filter = QuickInsert.filters.filters.find((f) => f.id === id);
      if (filter) filter.disabled = true;
      QuickInsert.filters.filtersChanged(FilterType.Client);
    });
  }

  editFilter(id: string): void {
    if (!this.filterEditors[id]) {
      const filter = QuickInsert.filters.filters.find((f) => f.id === id);
      if (filter) this.filterEditors[id] = new FilterEditor(filter);
    }

    this.filterEditors[id].render(true);
  }

  newFilter(original?: SearchFilter): void {
    const scope = `
  <p>
    <label>${i18n("FilterListFilterScope")}</label>
    <select>
      <option value="world">${i18n("FilterListFilterScopeWorld")}</option>
      <option value="client">${i18n("FilterListFilterScopeClient")}</option>
    </select>
  </p>`;
    const newDialog = new Dialog({
      title: original
        ? i18n("FilterListDuplicateFilterTitle", { original: original.tag })
        : i18n("FilterListNewFilterTitle"),
      content: `
        <div class="new-filter-name">
          @<input type="text" name="name" id="name" value="" placeholder="${i18n(
            "FilterListFilterTagPlaceholder"
          )}" pattern="[A-Za-z0-9\\._-]+" minlength="1">
        </div>
        ${game.user?.isGM ? scope : ""}
      `,
      buttons: {
        apply: {
          icon: "<i class='fas fa-plus'></i>",
          label: i18n("FilterListCreateFilter"),
          callback: async (
            html: JQuery<HTMLElement> | HTMLElement
          ): Promise<void> => {
            if (!("find" in html)) return;
            const input = html.find("input");
            const val = html.find("input").val() as string;
            const selected = html.find("select").val();
            if (input.get(0).checkValidity() && val !== "") {
              this.createFilter(
                val,
                selected === "world" ? FilterType.World : FilterType.Client,
                original
              );
            } else {
              ui.notifications?.error(`Incorrect filter tag: "${val}"`);
            }
          },
        },
      },
      default: "apply",
      close: (): void => {
        return;
      },
    });
    newDialog.render(true);
  }

  createFilter(tag: string, scope: FilterType, original?: SearchFilter): void {
    const newId = randomId(30);

    if (original) {
      QuickInsert.filters.addFilter({
        id: newId,
        type: scope,
        tag,
        subTitle: `${original.subTitle} (Copy)`,
        filterConfig:
          original.filterConfig && cloneFilterConfig(original.filterConfig),
      });
      return;
    } else {
      QuickInsert.filters.addFilter({
        id: newId,
        type: scope,
        tag,
        subTitle: tag,
        filterConfig: {
          compendiums: [],
          folders: [],
          entities: "any",
        },
      });
    }
    if (scope == FilterType.Client) {
      this.editFilter(newId);
    } else {
      Hooks.once("QuickInsert:FiltersUpdated", () => this.editFilter(newId));
    }
  }

  async _updateObject(): Promise<unknown> {
    return;
  }
}
