# Changelog

## [2.5.0] - 2021-08-22

### Added

- More API features and documentation (see README)

### Changed

- Big refactor for updated and deprecated 0.8.x functionality
- Minimum compatible version bumped to 0.8.8

### Fixed

- Filter editing works again.
- Sheet item submissions fixed (probably)

## [2.4.7] - 2021-06-01

Foundry 0.8.6 update, unfortunately custom keybinds will be reset.

### Changed

- Replaced Settings Extender with Keybind Lib
- Compatible with 0.8.6
- Dropped support for foundry versions older than 0.7.8

## [2.4.6] - 2021-04-27

Finally fixed the longest outstanding bug.

### Fixed

- Dynamic Active Effects are now correctly applied when using the search button
  on the character sheet integration! (only on 0.7.9 and later)

## [2.4.5] - 2021-04-20

Maintenance and user suggestions.

### Added

- Compatible with Forien's Quest Log (quests no longer shows up in search as
  journals)
- Keybind toggles Quick Insert window (closes the input if it's already open)

### Changed

- Allow the GM to set a custom indexing timeout (useful for slower servers or
  larger compendiums)

### Fixed

- Updated Spanish translation (thanks again José)
- Don't break if there's playlist compendiums
- Quick Insert keybind works even if the canvas failed to load
- Broken dnd5e search button formatting

## [2.4.4] - 2021-02-17

Random fixes

### Added

- PDFoundry support - open or insert PDFs links with Quick Insert (thanks to
  JeansenVaars)
- Re-added indexing on startup as an optional feature (default disabled)
- Optional search button in the scene controls

## [2.4.3] - 2021-01-25

Hotfix

### Changed

- Indexing on startup is disabled until further notice as it is deemed unsafe

## [2.4.2] - 2021-01-17

Fixes and refactoring. Rewriting the indexing one more time to avoid ENOENT
errors...

### Changed

- Indexing changed to a progressive approach
  - Eliminated repeated calls to getIndex on timeout
  - Progressively increasing index timers

### Fixed

- Character sheet integration in Tidy5e grid layout

## [2.4.1] - 2020-12-23

Small fixes and tweaks

### Added

- Selected text anywhere will now be pre-filled in Browse Mode

### Fixed

- Tuned retry mechanics for failed compendium indexing (again)

### API

- Added `QuickInsert.matchBoundKeyEvent()` function for matching the bound
  keyboard event (e.g. in a custom editor)

## [2.4.0] - 2020-12-06

New action system!

### Added

- Search results may now have multiple separate actions to perform
  - Scenes: View or Activate
  - Roll Tables: Roll or Show
  - Macros: Execute or Edit
  - Swap between available actions with <kbd>Tab</kbd> and submit with
    <kbd>Enter</kbd>
  - Or click the new action icons
- Roll tables can be rolled in insert mode, automatically inserting the result!

### Changed

- Big rewrite of the search results, rendering with Svelte
- Bumped compatibility to 0.7.8

## [2.3.7] - 2020-11-05

More compatibility

### Added

- Wait before loading the index. Configurable in settings
  - Including an option to wait with indexing until the first time the app is
    opened.

### Changed

- Use built-in `_onDropItemCreate` on 0.7.x when adding items to character
  sheets via the integrated search button.

### Fixed

- Updated Spanish translation, thanks again José!

## [2.3.6] - 2020-10-25

Smaller fixes

### Fixed

- Wait .5s before indexing when using the Foundry client (to avoid errors on
  startup)
- Saving settings in 0.7.x would break any keybind with `Space`, fixed for
  tinyMCE editors as well
- Avoid an error in Indexing Settings due to missing default value

## [2.3.5] - 2020-10-24

More fixing and tweaking

### Added

- First pass bulk editing for Indexing Settings: Enable/disable entire
  compendiums with one click, buttons to select/deselect all

### Changed

- Stopped automatically disabling compendiums that fail to index, they are
  simply ignored instead

### Fixed

- Rebuilding the index could duplicate filters in list

## [2.3.4] - 2020-10-23

Maintenance and localization

### Added

- Added Spanish localization thanks to José Lozano

### Changed

- Hoding shift while drag-dropping entities will no longer close the search
  window

### Fixed

- Changed indexing diagnostics to avoid false positives (no longer spamming
  "failed to index..." warnings)

## [2.3.3] - 2020-10-19

Major bug fixing

### Fixed

- Automatic diagnostics and disabling of compendiums that can't be indexed
  (usually corrupted `.db` file(s))
- Saving settings in 0.7.4 would break any keybind with `Space`
- Add experimental "Index Guard" to avoid the `ENOENT:...` errors (enable in
  settings)
- Fix bugs in the "Character Sheet Filters" menu
- The keybind would be disabled when switching scenes, _if_ the Terrain Layers
  module was activated

## [2.3.2] - 2020-10-14

### Added

- Added localization everywhere, Quick Insert is ready to be translated

### Changed

- Changing the indexing settings will force a full reindex for all connected
  users
  - The reindexing is staggered between users to avoid hitting the databases at
    the same time.

### Fixed

- Entity change errors if search lib initialization failed
- "Character sheet filters" menu is available again (if the system has character
  sheet integration)
- Disabling "Click result to submit" setting actually shows the sheet when you
  click results now
- If the keybind is <kbd>Ctrl</kbd> + <kbd>Space</kbd>: When using waypoints to
  move tokens, Quick Insert would open instead of moving the token. Quick Insert
  will now ignore the keybind if there are active waypoints.

## [2.3.1] - 2020-10-13

Robustness fixes

### Fixed

- Reordered initialization to avoid failures during startup
- If the indexing failed during startup, the index will be rebuilt first time
  when opening the search window (unless that also fails for some reason)

## [2.3.0] - 2020-10-10

Search more (or less) things!

### Added

- Added the ability to search for roll tables and scenes!
- New Indexing settings menu.

  - **Configure who can search for what**. Disable/enable entity types and
    compendiums.

### API Changes

- Items returned from search now have an async `get()`function that returns the
  actual object.
- Add `QuickInsert.search()` function that uses the internal search library and
  returns a list of search results.

## [2.2.1] - 2020-10-09

Hotfix

### Fixed

- Fix permission error for non-GMs on startup.

## [2.2.0] - 2020-10-05

System character sheet integration focus.

### Added

- Option to disable character sheet integration
- A basic settings menu to select which filters to use in character sheet
  integration
- SWADE character sheet integration
- WFRP4e character sheet integration
- Starfinder character sheet integration

## [2.1.1] - 2020-10-04

Minor fixes, more coming soon(ish).

### Added

- Added some more logging to troubleshoot initialization issues.

## Changed

- Upgrade to TypeScript 4

### Fixed

- Stop default behaviour on keybind in TinyMCE editors (prevents newline if
  enter is part of shortcut)
- Refocus TinyMCE editor when closing search
- Events from Dice Tray caused errors (it emulates keyboard events)
- Reset the image in rolltable if the new seleciton has none.

## [2.1.0] - 2020-09-05

Fixes and refactoring.

### Changed

- Targeted JavaScript version is now ES2020. If this breaks anything for users,
  let me know and I'll look at reverting.
- See API changes below.

### Fixed

- Search filters with folders now apply recursively (correctly including items
  in subfolders)
- Minor search focus UX behaviours
- Attempt to mitigate `Error: ENOENT: no such file or directory ...` happening
  when multiple modules try to fetch compendium indexes simultaneously (The
  error will still happen some times, but Quick Insert should no longer break.
  It will detect the error and retry every 2 seconds, for a maximum of 5
  attempts)
- Avoid doubled search buttons in Tidy5e Sheet (appeared in Foundry 7.x)
- dnd5e character sheet search will only show "Item" type entities, as anything
  else would be silly

### API Changes

See API usage added to the README!

- Major refactor of Quick Insert structure - now available as an importable
  singleton object instead of polluting the `ui` object
- Added optional fields to contexts (all fields on context are optional)
  - `classes` - a list of classes that will be added to the search app
  - `restrictTypes` - a list of entity types that the current context is
    restricted to (e.g. `restrictType: ["Item"]` to ensure that only Items will
    show up)
- Modularised system intergation. Using dynamic `import()` so that we only load
  code for the current system. System integrations are not bundled in the main
  `quick-insert.js`, so they can be imported directly by other modules.

## [2.0.1] - 2020-08-23

Minor fix

### Fixed

- Disabled filters showed up in a few cases

## [2.0.0] - 2020-08-23

New filter system with custom filters!

### Added

- Rewritten filter system

  - All filters are data driven
  - Custom filters with filter editor
  - Filter by entity type, collection or folder

- Insert at the cursor/selection for regular text inputs (chat/macros)
- Setting to enable Quick Insert for GMs only
- Probably some new bugs

### Changed

- Search behaviour may differ due to search index refactoring.

### Fixed

- Bug fixes

## [1.0.3] - 2020-07-29

Bug fixes

### Fixed

- shift+enter acts the same with/without filter (keeps search output, but clears
  input)
- Avoid some errors
- Disable spellcheck on input

## [1.0.2] - 2020-07-18

Bug fixes

### Fixed

- Missing uuid method for world entities stopped them from being inserted in
  character sheets.
- Context for chat type macros re-enabled, accidentally stopped working after
  adding context whitelist.

## [1.0.1] - 2020-07-18

Usability and bug fixes

### Added

- Quick Insert buttons for NPC sheets (dnd5e and Tidy5e)

### Fixed

- Fix for window accidentally closing after clicking a filter or shift-clicking
  an item.
- Fix a bug where the click handler fired multiple times, opening multiple
  windows or inserting mutliple entities.

## [1.0.0] - 2020-07-17

First release of Quick Insert!
